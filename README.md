# README #
Solutions.java is the main class. 

It leads to two solutions. 

## 1. SimpleSolution
This solution assumes that the string format will strictly follow the given sample. It uses regex to extracts the values using capture groups and then formats the output using String.format. Not very efficient, not extensible, but gets the job done quick and easy.

## 2. AdvancedSolution 
This solution assumes that the input is a string representation of an object tree. It tokenizes the input, parses the tokens and uses a eventing model to notify a parse listener. The parse listener constructs an in-memory model from the events it receives. This model is then serialized to the desired output using a couple of recursive functions. This solution allows modification and extension of the string format and the output format independent of each other. It also establishes a framework to do more interesting things with the same input.

**Note:** You can ignore all the gradle stuff in the project. It does not use anything other than java std libraries.