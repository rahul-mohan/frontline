package com.rahul.frontline.parsingsolution.serializer;

import java.util.Set;

import com.rahul.frontline.parsingsolution.model.Document;
import com.rahul.frontline.parsingsolution.model.NamedAttribute;

/**
 * Serializes a document tree into a hierarchical string format
 * @author rahul
 */
public class DocumentSerializer {

	//os specific line separator
	private static final String NEWLINE = System.lineSeparator();
	
	/**
	 * Serializes the given document tree to a String formatted as a left aligned tree representation
	 * @param doc
	 * 			The tree to be serialized
	 * @return
	 * 			Formatted string with values from the input tree
	 */
	public static String serializeToTree(Document doc) {
		if(doc==null)
			return "NULL";
		
		StringBuilder output = new StringBuilder();
		StringBuilder prefix = new StringBuilder(); //keeps track of the prefix, -, --, --- etc.
		
		serializeDocument(output,doc, prefix);
		
		return output.toString();
	}

	private static void serializeDocument(StringBuilder output, Document doc, StringBuilder prefix) {
		output
			.append(prefix)
			.append(doc.getName())
			.append(NEWLINE);
		
		if(prefix.length()==0)
			prefix.append(' '); //top level attributes are not prefixed with -
		else
			prefix.append('-');
		serializeAttributes(output,doc.getAttributes(), prefix);
		prefix.deleteCharAt(prefix.length()-1); //remove one nesting level
	}

	private static void serializeAttributes(StringBuilder output, Set<NamedAttribute> attribs, StringBuilder prefix) {
		
		for(NamedAttribute attrib: attribs) {
			
			if(attrib instanceof Document) {
				serializeDocument(output, (Document) attrib, prefix);
			} else {
				output
					.append(prefix)
					.append(attrib.getName())
					.append(NEWLINE);
			}
		}
	}
	
}
