package com.rahul.frontline.parsingsolution;

import java.util.Deque;
import java.util.LinkedList;

import com.rahul.frontline.parsingsolution.model.Document;
import com.rahul.frontline.parsingsolution.parser.DocumentParseEventHandler;


/**
 * An implementation of the DocumentParseEventHandler that builds an in-memory object tree from the events.
 * @author rahul
 */
class DocumentObjectModelBuilder implements DocumentParseEventHandler {

	/**
	 * Points to the root of the document tree
	 */
	private Document root;
	
	/**
	 * Points to the current processing context.
	 */
	private Document currentDoc;
	
	/**
	 * Stack used for tracking the processing context. 
	 * New objects are pushed to the stack so that all future events are applied to the top of the stack.
	 * After processing an object, the stack is popped so that the context falls back the parent object.
	 */
	private final Deque<Document> stack;
	
	public DocumentObjectModelBuilder(){
		stack = new LinkedList<>();
	}

	/**
	 * Called by parser when it encounters start of a new document
	 * @param name
	 * 			Name of the document
	 */
	@Override
	public void startDocument(String name) {
		
		Document newDocument = new Document(name);
		
		if(root==null) {
			//first document. so assign as root.
			root = newDocument;
		} else {
			//add as child to previous doc
			currentDoc.addAttribute(newDocument);
		}
		
		//push the new doc into stack
		if(currentDoc!=null)
			stack.push(currentDoc);
		
		//set context to the new doc
		currentDoc = newDocument;
	}
	
	/**
	 * Called by parser when it encounters end of a document
	 */
	@Override
	public void endDocument() {
		if(!stack.isEmpty()) {
			//reset context to the parent of the current doc
			currentDoc = stack.pop();
		}
	}

	/**
	 * Called by parser when it encounters an attribute of a document
	 * @param attribute
	 * 			Value of the attribute
	 */
	@Override
	public void addAttribute(String attribute) {
		//add attribute as child of the current doc
		if(attribute!=null && attribute.trim().length()!=0)
			currentDoc.addAttribute(attribute);
	}

	public Document getDocument() {
		return root;
	}
}
