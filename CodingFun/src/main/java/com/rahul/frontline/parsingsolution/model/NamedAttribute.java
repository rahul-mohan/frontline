package com.rahul.frontline.parsingsolution.model;

/**
 * Base class for all domain types
 * Implements Comparable to support sorting by name 
 * @author rahul
 */
public class NamedAttribute implements Comparable<NamedAttribute>{

	protected final String name;

	public NamedAttribute(final String name) {
		this.name = name;
	}

	@Override
	public int compareTo(NamedAttribute o) {
		if(o==null)
			return 1;
		
		return name.compareTo(o.name);
	}
	
	public String getName() {
		return name;
	}
}
