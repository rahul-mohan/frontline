package com.rahul.frontline.parsingsolution.parser;

/**
 * Event handler interface for listening to document parse events. Implementations can decide how to process these events.
 * @author rahul
 */
public interface DocumentParseEventHandler {

	/**
	 * Called by parser when it encounters start of a new document
	 * @param name
	 * 			Name of the document
	 */
	void startDocument(String name);

	/**
	 * Called by parser when it encounters end of a document
	 */
	void endDocument();

	/**
	 * Called by parser when it encounters an attribute of a document
	 * @param attribute
	 * 			Value of the attrribute
	 */
	void addAttribute(String attribute);

}
