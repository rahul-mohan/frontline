package com.rahul.frontline.parsingsolution.model;

import java.util.Set;
import java.util.TreeSet;

/**
 * Document domain class. Holds a sorted set of attributes. 
 * Some of the attributes can be other Document objects
 * @author rahul
 */
public class Document extends NamedAttribute {
	
	private final Set<NamedAttribute> attributes;
	
	public Document(final String name) {
		super(name);
		attributes = new TreeSet<>();//for sorted results
	}
	
	public void addAttribute(String name) {
		attributes.add(new NamedAttribute(name));
	}
	
	public void addAttribute(Document doc) {
		attributes.add(doc);
	}

	public Set<NamedAttribute> getAttributes() {
		return attributes;
	}
	
}
