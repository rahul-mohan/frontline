package com.rahul.frontline.parsingsolution;

import com.rahul.frontline.parsingsolution.model.Document;
import com.rahul.frontline.parsingsolution.parser.DocumentStringParser;
import com.rahul.frontline.parsingsolution.serializer.DocumentSerializer;

/**
 * A parser based solution for the given problem. 
 * This solution treats the input string as a serialized form of a (kind of) document object tree.
 * This is a generic and extensible solution that can handle extensions to the input grammar and the output grammar independently 
 * <p>
 * The model is as follows:
 * <ul>
 * <li>NamedProperty:class has property "name" </li>
 * <li>Document:class ---isA--> NamedProperty:class</li>
 * <li>Document:class ---hasOrderedListOf--> NamedProperty:class</li>
 * </ul>
 * </p>
 * @author rahul
 */
public final class AdvancedSolution {
	
	private AdvancedSolution() {
		//do not instantiate
	}
	
	/**
	 * 
	 * @param input
	 * 		  A string representation of a document object tree. The format is DocumentName(attrib1,attrib2,....), where attrib = a string OR attrib = a Document
	 * @return A string with values parsed from the input formatted in a hierarchical tree structure
	 */
	public static String parseAndFormat(String input) {
		
		//instantiate the parse event listener
		DocumentObjectModelBuilder docBuilder = new DocumentObjectModelBuilder();
		
		//intialize parser
		DocumentStringParser parser = new DocumentStringParser(docBuilder);
		
		//parse
		parser.parse(input);
		
		//get hold of the parsed in-memory object tree
		Document doc = docBuilder.getDocument();
		
		//serialize to tree format
		return DocumentSerializer.serializeToTree(doc);
	}
	
}
