package com.rahul.frontline.parsingsolution.parser;

/**
 * A parser for the document string format
 * @author rahul
 */
public class DocumentStringParser {
	
	//the parse event handler
	private final DocumentParseEventHandler listener;
	
	//buffer used to "remember" the previous token from the current parse position
	private StringBuilder tokenBuffer;
	
	public DocumentStringParser(final DocumentParseEventHandler listener) {
		this.listener = listener;
		tokenBuffer = new StringBuilder();
	}

	/**
	 * Parses the given input string and calls the associated DocumentParseEventHandler with each token
	 * @param input
	 * 			A string representation of a document object tree. The format is DocumentName(attrib1,attrib2,....), where attrib = a string OR attrib = a Document
	 */
	public void parse(String input){
		
		if(input==null || input.trim().length()==0)
			return;
		
		for(char c: input.toCharArray()) {
			
			switch(c) {
			
				//start of doc
				case '(':
					listener.startDocument(previousToken());
					clearPreviousToken();
					break;
				
				//end of doc
				case ')': 
					listener.addAttribute(previousToken());
					clearPreviousToken();
					listener.endDocument();
					break;
				
				//attribute separator
				case ',': 
					listener.addAttribute(previousToken());
					clearPreviousToken();
					break;
				
				//spaces are ignored
				case ' ': break;
				
				//actual alphanumeric tokens
				default: 
					addCharToToken(c);
					
			}
		}
		
	}
	
	/**
	 * Returns previous token
	 * @return
	 */
	private String previousToken() {
		return tokenBuffer.toString();
	}

	/**
	 * Adds the given char to current token
	 * @param c
	 */
	private void addCharToToken(char c) {
		tokenBuffer.append(c);
	}

	/**
	 * Clears the buffer
	 */
	private void clearPreviousToken(){
		tokenBuffer.delete(0, tokenBuffer.length());
	}
	
}


