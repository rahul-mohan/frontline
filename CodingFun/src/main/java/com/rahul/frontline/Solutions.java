package com.rahul.frontline;

import com.rahul.frontline.parsingsolution.AdvancedSolution;
import com.rahul.frontline.stringmanipulation.SimpleSolution;

public class Solutions {

	public static void main(String[] args) {
		String input = "(id,created,employee(id,firstname,employeeType(id),lastname),location)";
		
		//solution-1: string manipulation
		String output = SimpleSolution.parseAndFormat(input);
		System.out.println("Simple Solution Output:\n" + output);
		
		//solution-2: parsing
		output = AdvancedSolution.parseAndFormat(input);
		System.out.println("\n=======\nAdvanced Solution Output:\n" + output);
	}
	
	
}
