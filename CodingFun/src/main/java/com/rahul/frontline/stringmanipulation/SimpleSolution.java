package com.rahul.frontline.stringmanipulation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Simple solution that meets the given requirements. 
 * 
 * <p><b>Assumption:</b> The input string will have the values always at the exact same spot. Changing the position of words in the input can produce incorrect output.</p>
 * <p>
 * <b>Limitations:</b>
 * <ol>
 * <li>Not tolerant to variations in the input</li>
 * <li>Not efficient due to usage of Regex and String.format</li>
 * <li>This solution leverages string manipulation using regex, the syntactic structure of the input is not at all leveraged. 
 * This makes it useless for any further processing from the input </li>
 * <li>Hard to debug and maintain for someone who doesn't know regex very well.</li>
 * <li>Producing output in alphabetical order is not straight forward since the structure of the data is not available from the capture groups in regex</li>
 * </ol>
 * </p>
 * @author rahul
 */
public final class SimpleSolution {

	/**
	 * Expected pattern of the input. \\w is same as [a-zA-Z0-9]
	 */
	private static final Pattern INPUT_PATTERN = Pattern.compile("\\((\\w+),(\\w+),(\\w+)\\((\\w+),(\\w+),(\\w+)\\((\\w+)\\),(\\w+)\\),(\\w+)\\)");
	
	private static final String OUTPUT_FORMAT = "%s\n%s\n%s\n-%s\n-%s\n-%s\n--%s\n-%s";
	
	private SimpleSolution() {
		//do not instantiate this class
	}
	
	
	/**
	 * Parses the given input string according to the <code>INPUT_PATTERN</code> regular expression. 
	 * Prints the values extracted from the capture groups in a hierarchical format.
	 * @param input
	 * 		  The string to be parsed
	 * @return A string containing all the values extracted from the input string formatted in a hierarchical structure
	 * @throws java.lang.IllegalArgumentException
	 * 		   if the input string does not match the expected format
	 */
	public static String parseAndFormat(String input) {
		Matcher matcher = INPUT_PATTERN.matcher(input);
		if(!matcher.matches()) {
			throw new IllegalArgumentException("Bad format: " + input);
		} 
		
		String id = matcher.group(1);
		String created = matcher.group(2);
		String employee = matcher.group(3);
		String employeeId = matcher.group(4);
		String employeeFirstName = matcher.group(5);
		String employeeType = matcher.group(6);
		String employeeTypeId = matcher.group(7);
		String employeeLastName = matcher.group(8);
		String location = matcher.group(9);
				
		
		return String.format(OUTPUT_FORMAT, 
				id, 
				created,
				employee,
				employeeId,
				employeeFirstName,
				employeeType,
				employeeTypeId,
				employeeLastName,
				location);
	}
}
